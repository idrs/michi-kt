import sun.reflect.generics.tree.Tree
import kotlin.coroutines.experimental.buildSequence
import kotlin.math.min
import kotlin.math.roundToInt

var size: Int = 19
val width: Int
    get() = size + 2
val emptyLine = ".".repeat(size)
val emptyBoard = " ".repeat(size + 1) + "\n" + (" " + ".".repeat(size) + "\n").repeat(size) + " ".repeat(size + 1) + "\n"
val emptyGroupArray = List(width * width) { 0 }
val emptyMapIntSetInt = mapOf<Int, Set<Int>>()

val colstr = "ABCDEFGHJKLMNOPQRST"

val priorEven = 10.0 //should be even number; 0.5 prior

val maxGameLen = size * size * 3

val probHeuristic = Pair(0.9,0.95) // probability of heuristic suggestions being taken in playout
val probSsareject = 0.9  // probability of rejecting suggested self-atari in playout
val probRsareject = 0.5 // probability of rejecting random self-atari in playout; this is lower than above to allow nakade



val pat3src = arrayOf(  // 3x3 playout patterns; X,O are colors, x,o are their inverses
arrayOf("XOX",  // hane pattern - enclosing hane
"...",
"???"),
arrayOf("XO.",  // hane pattern - non-cutting hane
"...",
"?.?"),
arrayOf("XO?",  // hane pattern - magari
"X..",
"x.?"),
// arrayOf("XOO",  // hane pattern - thin hane
//  "...",
//  "?.?", "X",  - only for the X player
arrayOf(".O.",  // generic pattern - katatsuke or diagonal attachment; similar to magari
"X..",
"..."),
arrayOf("XO?",  // cut1 pattern (kiri) - unprotected cut
"O.o",
"?o?"),
arrayOf("XO?",  // cut1 pattern (kiri) - peeped cut
"O.X",
"???"),
arrayOf("?X?",  // cut2 pattern (de)
"O.O",
"ooo"),
arrayOf("OX?",  // cut keima
"o.O",
"???"),
arrayOf("X.?",  // side pattern - chase
"O.?",
"   "),
arrayOf("OX?",  // side pattern - block side cut
"X.O",
"   "),
arrayOf("?X?",  // side pattern - block side connection
"x.O",
"   "),
arrayOf("?XO",  // side pattern - sagari
"x.x",
"   "),
arrayOf("?OX",  // side pattern - cut
"X.O",
"   "))

//val patGridularSeq =


val spatPatterndictFile = "patterns.spat"
val largePatternsFile = "patterns.prob"

fun neighbors(c: Int): List<Int> {
    return listOf(c - width, c - 1, c + 1, c + width)
}

fun diagNeighbors(c: Int): List<Int> {
    return listOf(c - width - 1, c - width + 1, c + width - 1, c + width + 1)
}

fun Char.swapcase() = if (this.isUpperCase()) this.toLowerCase() else this.toUpperCase()
fun String.swapcase() = this.map { it.swapcase() }.joinToString("")

class Board(
        val goban: String = emptyBoard,
        val groupArray: List<Int> = emptyGroupArray,
        val groups: Map<Int, Set<Int>> = emptyMapIntSetInt,
        val liberties: Map<Int, Set<Int>> = emptyMapIntSetInt
) {

    fun swapcase() = Board(goban.swapcase(), groupArray, groups, liberties)

    override fun toString(): String {
        val groupArrayStr = groupArray.mapIndexed { i, d ->
            if (goban[i].isWhitespace()) {
                goban[i].toString()
            } else d.toString()
        }.map { it.padStart(2, ' ') }.joinToString(separator = " ")
        return goban + "\n" + groupArrayStr

    }
}


fun boardPut(
        board: Board,
        c: Int,
        p: Char,
        n: Int
): Board {
    val newBoard = board.goban.take(c) + p + board.goban.drop(c + 1)

    val ngh = neighbors(c)
    val libsOfC = ngh.filter { board.goban[it] == '.' }

    /* update groups */
    val sameColorNgh = ngh.filter { board.goban[it] == p }
    val otherColorNgh = ngh.filter { board.goban[it] == p.swapcase() }
    val listOfNghGroups = sameColorNgh.map { board.groupArray[it] }
    val listOfOtherGroups = otherColorNgh.map { board.groupArray[it] }
    val newGroup = listOfNghGroups.mapNotNull { board.groups[it] }
            .fold(setOf(c), { acc: Set<Int>, nxt: Set<Int> -> acc + nxt })
    val newGroups = board.groups - listOfNghGroups + Pair(n, newGroup)
    /* Should we not remove old groups ?:
    val newGroups = groups + Pair(n,newGroup)
    */

    val newGroupArray = board.groupArray
            .mapIndexed { i: Int, elt: Int -> if (elt in listOfNghGroups || i == c) n else elt }
    val newGroupLiberties = board.liberties
            .filterKeys { k -> k in listOfNghGroups }
            .map { (k, v) -> v }
            .fold(emptySet(), { acc: Set<Int>, l: Set<Int> -> acc + l }) - c + libsOfC
    val otherGroupLiberties = board.liberties
            .filterKeys { it in listOfOtherGroups }
            .mapValues { (k, v) -> v - c }
    val newLiberties = board.liberties + Pair(n, newGroupLiberties) + otherGroupLiberties - listOfNghGroups

    return Board(newBoard, newGroupArray, newGroups, newLiberties)
}

fun atariUpdate(board: Board, grps: List<Int>): Board {
    val removedStones = grps.mapNotNull { board.groups[it] }.flatten()
    val newBoard = board.goban.mapIndexed { i: Int, c: Char -> if (i in removedStones) '.' else c }.joinToString("")
    val newGroupArray = board.groupArray.mapIndexed { i: Int, g: Int -> if (i in removedStones) 0 else g }
    val newGroups = board.groups - grps
    val newGrpAndLib = removedStones.map { c: Int ->
        neighbors(c).filter { board.goban[it] == 'X' }.map { Pair(board.groupArray[it], c) }
    }.flatten()

    val newLiberties = newGrpAndLib.fold(
            initial = board.liberties,
            operation = { acc: Map<Int, Set<Int>>, p: Pair<Int, Int> -> acc + Pair(p.first, acc.getOrDefault(p.first, setOf()) + p.second) }
    )
    return Board(newBoard, newGroupArray, newGroups, newLiberties)
}


fun isEyeish(board: Board, c: Int): Char? {
    /**
     * Test if c is inside a single-color diamond and return the diamond color or null;
     * this could be an eye but also a false one.
     */
    val ngh = neighbors(c).map { board.goban[it] }.filterNot { it.isWhitespace() }
    return when {
        '.' in ngh -> null
        ngh.size > 1 -> null
        else -> ngh.first()
    }
}

fun isEye(board: Board, c: Int): Char? {
    /**
     * test if c is an eye and return its color or Non
     */
    val eyecolor = isEyeish(board, c)
    val falsecolor = eyecolor?.swapcase() ?: return null
    val (edges, diagNgh) = diagNeighbors(c).map { board.goban[it] }.partition { it.isWhitespace() }
    val atEdge = if (edges.isNotEmpty()) 1 else 0
    return if (atEdge + diagNgh.filter { it == falsecolor }.size > 1) null
    else eyecolor
}

data class Position(val board: Board, val cap: Pair<Int, Int>, val n: Int, val ko: Int?, val last: Int?, val last2: Int?, val komi: Double)

/**
 * Implementation of simple Chinese Go rules;
 * n is how many moves were played so far
 */

fun move(pos: Position, c: Int): Position? {
    /** play as player X at the given coord c, return the new position */
    val board = pos.board
    /* Test for ko */
    if (c == pos.ko) return null

    /* In enemy' eye ? */
    val inEnemyEye = isEyeish(board, c) == 'x'

    val newboard = boardPut(board, c, 'X', pos.n)
    /* test for captures */
    val (capX, capx) = pos.cap
    val captGrp = neighbors(c)
            .filter { newboard.goban[it] == 'x' }
            .map { newboard.groupArray[it] }
            .filter { newboard.liberties.getOrDefault(it, setOf()).isEmpty() }
    val captStones = captGrp.mapNotNull { newboard.groups[it] }
    val singleCaps = captStones.filter { it.size == 1 }.map { it.first() }
    val newCapX = capX + captStones.flatten().size
    val cleanedBoard = atariUpdate(newboard, captGrp)
    val ko: Int? = if (inEnemyEye && singleCaps.size == 1) singleCaps.first() else null

    /* Test for suicide */
    return if (neighbors(c).map { newboard.goban[it] }.none { it == '.' }) null
    else Position(cleanedBoard.swapcase(), Pair(capx, newCapX), pos.n + 1, ko, c, pos.last, pos.komi)
}

fun passMove(pos: Position) = Position(
        board = pos.board.swapcase(),
        cap = Pair(pos.cap.second, pos.cap.first),
        n = pos.n + 1,
        ko = null,
        last = null,
        last2 = pos.last,
        komi = pos.komi
)

fun moves(pos: Position, i0: Int) = buildSequence{
    /**
     * Generate a list of moves (includes false positives - suicide moves;
     * does not include true-eye-filling moves), starting from a given board
     * index (that can be used for randomization)
     */
    yieldAll(pos.board.goban.withIndex().drop(i0).filter{it.value == '.' && isEye(pos.board,it.index)!='X'}.map{it.index})
    yieldAll(pos.board.goban.withIndex().take(i0).filter{it.value == '.' && isEye(pos.board,it.index)!='X'}.map{it.index})
}

fun lastMovesNeighbors(pos: Position): List<Int> {
    /** generate a randomly shuffled list of points including and
     * surrounding the last two moves (but with the last move havin
     * priority)
     */
    return listOf(pos.last,pos.last2).filterNotNull().map { (neighbors(it)+diagNeighbors(it) + it).shuffled() }.flatten().toSet().toList()
}

fun score(pos: Position, owner_map: Array<Int>? = null): Double {
    /** compute score for to-play player; this assumes a final position
     *  with all dead stones captured; if owner_map is passed, it is assumed
     *  to be an array of statistics with average owner at the end of the game
     *  (+1 black, -1 white)
     */
    /* determine empty groups */
    var freeGroups = Array(width * width) { -1 }
    val updateMap = pos.board.goban.foldIndexed(
            initial = emptyMap<Int, Int>(),
            operation = { i, acc, c ->
                if (c == '.') {
                    val pointAndNgh = listOf(i) + neighbors(i).filter { pos.board.goban[it] == '.' }
                    val minOfGrp = pointAndNgh.min() ?: i
                    val gpNb = if (freeGroups[minOfGrp] != -1) freeGroups[minOfGrp] else minOfGrp
                    val newAcc = pointAndNgh.fold(
                            initial = acc,
                            operation = { acc2, d ->
                                val g = freeGroups[d]
                                if (g == -1 || g == gpNb) {
                                    freeGroups[d] = gpNb;
                                    acc2
                                } else {
                                    acc2 + Pair(g, gpNb)
                                }
                            })
                    newAcc
                } else acc
            })
    //println(pos.board.goban.mapIndexed{i,c->(if(c.isWhitespace())c.toString() else freeGroups[i].toString()).padStart(2)})
    //println("updateMap done")
    //updateMap.forEach{(k,v)->println(k.toString()+","+v.toString())}
    tailrec fun chainedCalls(map: Map<Int, Int>, i: Int): Int = if (map.containsKey(i)) chainedCalls(map, map.getOrDefault(i,-1)) else i
    val updatedFreeGroups = freeGroups.map { chainedCalls(updateMap, it) }

//    println("updatedFreeGroups done")
    val colorMap:Map<Int,Pair<List<Int>,Char>> = updatedFreeGroups
            .withIndex()
            .groupBy(keySelector = { it.value }, valueTransform = { it.index })
            .filterKeys { it!=-1 }
            .mapValues { (key, value) ->
                val neighColors: List<Char> = value.flatMap { neighbors(it) }.map { pos.board.goban[it] }.toSet().filter { it in "xX" }
                value to (if (neighColors.size == 1) neighColors.first() else ':')
            }
    //println("colorMap done")
    val filledGoban = pos.board.goban.mapIndexed { i, c ->
        if (c != '.') c
        else {
            colorMap[freeGroups[i]]?.second
        }
    }
    //println("filledGoban done")
    val count = filledGoban.map{when(it){
        'X' -> 1
        'x' -> -1
        else -> 0
    }}.sum()
    //println("count done")
    if (owner_map != null) {
        val parity = if (pos.n % 2 == 0) 1 else -1
        filledGoban.forEachIndexed{i,c -> when(c){
            'X' -> owner_map[i]+=parity
            'x' -> owner_map[i]-=parity
        }}
    }
    val komi = if (pos.n % 2 == 1) pos.komi else -pos.komi
    return count.toDouble() + komi
}

fun emptyArea(board: Board, c: Int, dist: Int = 3):Boolean{
    /**
     * Check whether there are any stones in Manhattan distance up
     * to dist
     */
    return (1..dist)
            .fold(initial = setOf(c),
            operation = { acc: Set<Int>, _: Int ->
                acc.flatMap { neighbors(it) }.filterNot { board.goban[it].isWhitespace() }.toSet()
            })
            .map{board.goban[it]}
            .all{it == '.'}
}

fun Position.toString(ownerMap : Array<Int>? = null):String{
    /**
     * print visualization of the given board position, optionally also
     * including an owner map statistic (probability of that area of board
     * eventually becoming black/white)
     */
    val toplay = n%2 == 0
    val theBoard = if(toplay)board.goban.replace('x','O') else board.goban.replace('X','O').replace('x','X')
    val (Xcap,Ocap) = if(toplay) cap else Pair(cap.second,cap.first)


    val start = "Move: $n Black: $Xcap White: $Ocap Komi: $komi \n $theBoard"
    return start

}

/**
 * 3x3 pattern routines (those patterns stored in pat3src above)
 */

fun pat3expand(pat:Array<String>):List<String>{
    /**
     * All possible neighborhood configurations matching a given pattern;
     * used just for a combinatoric explosion when loading them in an
     * in-memory set
     */
    fun rot90(p:Array<String>) = arrayOf(""+p[2][0] + p[1][0] + p[0][0],""+ p[2][1] + p[1][1] + p[0][1],""+ p[2][2] + p[1][2] + p[0][2])
    fun vertFlip(p:Array<String>) = arrayOf(p[2], p[1], p[0])
    fun horizFlip(p:Array<String>) = p.map{it.reversed()}.toTypedArray()
    fun swapcolors(p:Array<String>)= p.map{it.replace('X', 'Z').replace('x', 'z').replace('O', 'X').replace('o', 'x').replace('Z', 'O').replace('z', 'o')}.toTypedArray()
    fun wildexp(p:String,c:Char,to:CharSequence):List<String> {
        return if (p.contains(c)){to.map{p.replaceFirst(c,it)}.flatMap{wildexp(it,c,to)}}
        else listOf(p)
    }
    fun wildcards(p:String) = wildexp(p,'?',".XO ").flatMap{wildexp(it,'x',".O ")}.flatMap{wildexp(it,'o',".X ")}
    fun apply(p:Array<String>,f: (Array<String>) -> Array<String>  )=listOf(p,f(p))
    return apply(pat, {rot90(it)})
            .flatMap{apply(it,{p->vertFlip(p)})}
            .flatMap{apply(it,{p->horizFlip(p)})}
            .flatMap{apply(it,{p->swapcolors(p)})}
            .flatMap{wildcards(it.joinToString(""))}
}

val pat3set = pat3src.flatMap{pat3expand(it)}.map{it.replace('O','x')}

fun neighborhood33(board: Board, c: Int):String{
    /**
     * return a string containing the 9 points forming 3x3 square around
     * a certain move candidate
     */
    return (board.goban.substring(c-width-1..c-width+1)+board.goban.substring(c-1..c+1)+board.goban.substring(c+width-1..c+width-1)).replace('\n',' ')
}

/**
 * large-scale pattern routines (those patterns living in patterns.{spat,prob} files)
 *
 * # are you curious how these patterns look in practice? get
 * # https://github.com/pasky/pachi/blob/master/tools/pattern_spatial_show.pl
 * # and try e.g. ./pattern_spatial_show.pl 71
 */

val spatPatternDict = hashMapOf<Int,String>()
/*
fun loadSpatPAtternDict(f:String){
    /**
     * load dictionary of positions, translating them to numeric ids
     */
    File(f).forEachLine {  }
    }
*/
/**
Go Heuristics
 */


fun lineHeight(c: Int): Int {
    val col = c.rem(width)
    val row = c.div(width)
    return listOf(col, row, width - col, width - row).reduce { acc, i -> min(acc, i) }
}

fun fixAtari(pos: Position, c: Int, singlePtOk: Boolean = false, twolibTest: Boolean = true, twolibEdgeOnly: Boolean = false): Pair<Boolean, List<Int>> {
    /**
     *An atari/capture analysis routine that checks the group at c,
     * determining whether (i) it is in atari (ii) if it can escape it,
    either by playing on its liberty or counter-capturing another group.
    N.B. this is maybe the most complicated part of the whole program (sadly);
    feel free to just TREAT IT AS A BLACK-BOX, it's not really that
    interesting!
     * The return value is a tuple of (boolean, \[coord..\]), indicating whether
    the group is in atari and how to escape/capture (or [] if impossible).
    (Note that (False, [...]) is possible in case the group can be captured
    in a ladder - it is not in atari but some capture attack/defense moves
    are available.)
    singlept_ok means that we will not try to save one-point groups;
    twolib_test means that we will check for 2-liberty groups which are
    threatened by a ladder
    twolib_edgeonly means that we will check the 2-liberty groups only
    at the board edge, allowing check of the most common short ladders
    even in the playouts
     */
    fun readLadderAttack(pos: Position, c: Int, l1: Int, l2: Int): Int? {
        /**
         * check if a capturable ladder is being pulled out at c and return
        a move that continues it in that case; expects its two liberties as
        l1, l2  (in fact, this is a general 2-lib capture exhaustive solver)
         */
        return listOf(l1, l2)
                .filter { l: Int ->
                    move(pos, l)?.let {
                        fixAtari(it, c, twolibTest = false)
                    }?.let {
                        it.first && it.second.isEmpty()
                    } ?: false
                }.firstOrNull()
    }

    val grp = pos.board.groupArray[c]
    val groupSize = pos.board.groups[grp]?.size ?: 0
    if (singlePtOk && groupSize == 1) return Pair(false, emptyList())
    else {
        val liberties = pos.board.liberties[grp]?.toList() ?: listOf()
        /* cardinal of liberties */
        if (liberties.size > 2) return false to listOf()
        else {
            if (liberties.size == 2 && twolibTest && groupSize > 1 && (!twolibEdgeOnly || liberties.map { lineHeight(it) }.all { it == 1 })) {
                return Pair(false, listOfNotNull(readLadderAttack(pos, c, liberties[0], liberties[1])))
            } else { /* in atari */
                if (pos.board.goban[c] == 'x') {
                    return true to liberties
                } else {
                    /* Before thinking about defense, what about counter-capturing a neighboring group? */
                    val nghOpponentGrp = pos.board.groups[grp]?.map { neighbors(it) }?.flatten()
                            ?.map { pos.board.groupArray[it] }?.toSet() ?: emptySet<Int>()-0-grp
                    val ccSolutions = nghOpponentGrp
                            .map { pos.board.groups[it] }
                            .mapNotNull { it?.first() }
                            .map { fixAtari(pos, it, twolibTest = false) }
                            .filter { it.first and it.second.isNotEmpty() }
                            .map { it.second }
                            .flatten()
                    /* we are escaping. Will playing our last liberty gain at least two liberties?  Re-floodfill to account for connecting*/
                    val escPoint = liberties.first()
                    val escPos = move(pos, escPoint) ?: return true to ccSolutions /* suicidal move ? */
                    val escLib = escPos.board.liberties[escPos.board.groupArray[escPoint]]?.toList() ?: emptyList()
                    val newSol = when {
                        escLib.size == 1 -> listOf<Int>()
                        escLib.size == 2 -> if (ccSolutions.isNotEmpty() || readLadderAttack(escPos, escPoint, escLib[0], escLib[1]) == null) listOf(escPoint)
                        else listOf()
                        else -> listOf(escPoint)
                    }
                    return true to (ccSolutions + newSol)
                }
            }
        }
    }
}

fun cfg_distances(board: Board, c: Int): Array<Int> {
    tailrec fun fringe(map: Array<Int>, visited: Set<Int>, visiting: Set<Int>, dist: Int): Array<Int> {
        if (visiting.isEmpty()) return map
        else {
            val neigh = visiting.map { neighbors(it) }.flatten().filterNot { board.goban[it].isWhitespace() || it in (visited + visiting) }.toSet()
            val extended = neigh.map { board.groupArray[it] }.filter { it != 0 }.map {
                board.groups[it]?.toList() ?: emptyList()
            }.flatten().toSet()
            visiting.forEach { map[it] = dist }
            val nexts = neigh + extended
            return fringe(map, visited + visiting, nexts, dist + 1)
        }
    }

    val map = Array(width * width) { -1 }
    return fringe(map, setOf(), setOf(c), 0)
}

fun parseCoord(coord: String): Int? {
    if (coord == "pass") return null
    else {
        return width + 1 + (size - coord.drop(1).toInt()) * width + colstr.indexOf(coord.take(1).toUpperCase())
    }
}

fun strCoord(c:Int?):String{
    if(c==null) return "pass"
    else{
        val row = (c-width+1)/width
        val col = (c-(width+1)) % width
        return "${colstr[col]}${size-row}"
    }
}

/**
 * montecarlo playout policy
 */

fun genPlayoutMoves(pos:Position, heuristicSet: List<Int>, probs : Pair<Double,Double> = Pair(1.0,1.0), expensiveOk : Boolean = false):Sequence<Pair<Int,String>> =

    buildSequence {
        /**
         * Yield candidate next moves in the order of preference; this is one
         * of the main places where heuristics dwell, try adding more!
         * heuristic_set is the set of coordinates considered for applying heuristics;
         * this is the immediate neighborhood of last two moves in the playout, but
         * the whole board while prioring the tree.
         */
        if (Math.random() <= probs.first) {
            val alreadySuggested = mutableSetOf<Int>()
            yieldAll(heuristicSet.filter { pos.board.goban[it] in "xX" }
                    .flatMap { c ->
                        val (inAtari, ds) = fixAtari(pos, c, twolibEdgeOnly = !expensiveOk)
                        val newds = ds.filterNot { it in alreadySuggested }.shuffled()
                        alreadySuggested.addAll(newds);
                        newds.map { Pair(it, "capture " + c.toString()) }
                    })

        }
        if (Math.random() <= probs.second) {
            val alreadySuggested = mutableSetOf<Int>()
            yieldAll(heuristicSet
                    .filter { pos.board.goban[it] == '.' && (it !in alreadySuggested) && (neighborhood33(pos.board, it) in pat3set) }
                    .map { c -> alreadySuggested.add(c); c to "pat3" })
        }

        /**
         *  Try *all* available moves, but starting from a random point
         */
        val x = (Math.random() * size + 1).roundToInt()
        val y = (Math.random() * size + 1).roundToInt()
        yieldAll(moves(pos, y * width + x).map{it to "random"})
    }


fun mcplayout(pos: Position, amafMap: Array<Int>, disp: Boolean = false):Triple<Double,Array<Int>,Array<Int>>{
    /**
     * Start a Monte Carlo playout from a given position,
     * return score for to-play player at the starting position;
     * amaf_map is board-sized scratchpad recording who played at a given
     * position first
     */
    if(disp) System.err.println("** SIMULATION **")
    val startN = pos.n
    tailrec fun playout(posA: Position,amafMap2:Array<Int>):Pair<Position,Array<Int>>{
        if(disp) System.err.println(posA.toString())
        /**
         * We simply try the moves our heuristics generate, in a particular
         * order, but not with 100% probability; this is on the border between
         * "rule-based playouts" and "probability distribution playouts".
         */
        val posAndckind = genPlayoutMoves(posA,lastMovesNeighbors(posA),probHeuristic)
                .mapNotNull { (c,kind) ->
                    if (disp && kind != "random") System.err.println("move suggestion " + strCoord(c) + " " + kind)
                    move(posA, c)?.to(Pair(c,kind))
                }
                .dropWhile { (pos,ckind) ->
                    val (c,kind) = ckind
                    (Math.random() <= (if(kind=="random") probRsareject else probSsareject) )
                            &&( fixAtari(pos,c,singlePtOk = true,twolibEdgeOnly = true).let{if(it.second.isEmpty()){
                                        if(disp) System.err.println("rejecting self-atari move ${strCoord(c)}"); true} else false
                                    })
                }
                .firstOrNull()
        val (posB:Position,ckind:Pair<Int,String>?) = posAndckind ?: Pair(passMove(posA),null)
        if (posB.last == null && posB.last2 == null) return posB to amafMap2
        else{
            val c = ckind?.first
            if(c != null && amafMap2[c]==0){
                amafMap2[c] = if(posA.n % 2 == 0) 1 else -1
            }
            return playout(posB,amafMap2)
        }
    }

    val (finalPos,finalAmafMap) = playout(pos,amafMap)
    var ownerMap = Array(width*width){0}
    var score = score(finalPos,ownerMap)
    if(disp) System.err.println("** SCORE B${if(finalPos.n %2 == 0)score else (-score)} **")
    if( (finalPos.n-startN)%2 != 0 ) score = -score
    return Triple (score , finalAmafMap , ownerMap )
}


/**
 * Tree class for MonteCarlo TreeSearch (MCTS)
 */

class TreeNode(val pos: Position,val v: Int = 0,val w: Int = 0,val pv: Double = priorEven,val pw: Double = priorEven/2,val av: Int = 0,val aw: Int = 0,var children: Array<TreeNode>? = null):Comparable<TreeNode> {
    /**
     * Monte-Carlo tree node;
     * v is #visits, w is #wins for to-play (expected reward is w/v)
     * pv, pw are prior values (node value = w/v + pw/pv)
     * av, aw are amaf values ("all moves as first", used for the RAVE tree policy)
     * children is None for leaf nodes
     */
  /*  fun expand(){
        /**
         * add and initialize children to a leaf node
         */
        val cfgMap: Array<Int> = if (pos.last != null) cfg_distances(pos.board,pos.last) else null
        children = emptyArray<TreeNode>()
    }*/

    override operator fun compareTo(node: TreeNode) = v - node.v

    fun bestMove():TreeNode?{
        /**
         * Best move is the most simulated one
         */
        return children?.max()
    }
}
fun main(args: Array<String>) {
    val testBoard = Board()

    println(testBoard.goban)
    println(testBoard.toString())
    println(testBoard.goban.length)
    println(width * width)
    val cfgdist = cfg_distances(testBoard, 47)
    val cfgstring = cfgdist.mapIndexed { i, d ->
        if (d == -1) {
            testBoard.goban[i].toString()
        } else d.toString()
    }.map { it.padStart(2, ' ') }.joinToString(separator = " ")
    /*println(cfgstring)*/
    val testPos = Position(Board(), 0 to 0, 1, null, null, null, 6.5)
    val pos = listOf("D4", "Q16", "D16", "Q4", "D10", "Q10", "K4", "K16", "K10").fold(testPos, { acc: Position, c: String ->
        val co = parseCoord(c); if (co == null) acc else move(acc, co) ?: acc
    })
    if(pos != null) {
        println(pos.board.toString())
        println(fixAtari(pos, 88))
        println(pos.board.goban)
        println(score(pos, null))
        println(pat3expand(arrayOf("Xx.", "oO ", "..x")))
    }
}

